"use client";

const DeleteUser = (props) => {
  console.log(props.id);
  const Userid = props.id;
  const deletee = async () => {
    let data = await fetch("http://localhost:3000/api/contacts/" + Userid, {
      method: "delete",
    });
    data = await data.json();
    if (data.success) {
      alert("data deleted");
    }
  };
  return <button onClick={deletee}>Delete</button>;
};

export default DeleteUser;
