import mongoose from "mongoose";

const contactSchema = new mongoose.Schema({
  username: {
    type: String,
  },
  phone: {
    type: String,
  },
},
  {
    timestamps:true,
  }
);


const ContactUser = mongoose.models.ContactUser || mongoose.model('ContactUser', contactSchema)

export default  ContactUser;