"use client"

import { addUserData } from "@/app/redux/slice";
import { useState } from "react";
import { useDispatch } from "react-redux";

const page = () => {
  const [name, setname] = useState("");
  const [city, setcity] = useState("");
  const dispatch = useDispatch();

  const addUser =  () => {
    try {
      dispatch(addUserData({ name, city }));
      alert("User added successfully!");
    } catch (error) {
      console.error("Error adding user:", error);
    }
  };

  return (
    <>
      <div className="container mx-auto px-4">
        <h1 className="text-center font-bold text-xl mb-4">Add Data</h1>
        <label>Name:</label>
        <input
          className="border"
          value={name}
          type="text"
          placeholder="enter Name"
          onChange={(e) => setname(e.target.value)}
        />
        <div className="mt-3">
          <label>city:</label>
          <input
            className="border"
            value={city}
            type="text"
            placeholder="enter city"
            onChange={(e) => setcity(e.target.value)}
          />
        </div>

        <button
          className="border-2 rounded-lg px-2  py-1 my-4 border-black border-solid"
          onClick={addUser}
        >
          Add User
        </button>
      </div>
    </>
  );
};

export default page;
