"use client";

import Link from "next/link";
import { useState } from "react";

const ContactForm = () => {
  const [user, setuser] = useState({
    username: "",
    phone: "",
  });

  const handlechange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setuser((predata) => ({ ...predata, [name]: value }));
  };

  const [status, setstatus] = useState(null);
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await fetch("/api/contacts", {
        method: "POST",
        headers: { Content_Type: "application/json" },
        body: JSON.stringify({
          username: user.username,
          phone: user.phone,
        }),
       
      });
        
      if (response.status === 200) {
        setuser({
          username: "",
          phone: "",
        });
        setstatus("success");
      } else {
        setstatus("error");
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="container text-center mx-auto px-4">
    <form
      className="md:px-20 px-10 md:py-16 py-5 mt-5 text-left border-[1px] md:w-96"
      onSubmit={handleSubmit}
    >
      <div className="mb-6">
        <label htmlFor="username" className="block capitalize text-lg">
          Enter Your Name
        </label>
        <input
          type="text"
          name="username"
          id="username"
          className="mt-2 py-3 px-4 rounded-md border-[1px]"
          placeholder="Enter Your Name"
          value={user.username}
          onChange={handlechange}
       
        ></input>
      </div>
   
      <div className="mb-6">
        <label htmlFor="phone" className="block capitalize text-lg">
          Phone Number
        </label>
        <input
          type="number"
          name="phone"
          id="phone"
          className="mt-2 py-3 px-4 rounded-md border-[1px]"
          placeholder="Enter phone-number"
          autoComplete="off"
          value={user.phone}
          onChange={handlechange}
   
        ></input>
      </div>
    
      {status === "success" && (
        <p className="text-green-400 font-semibold mb-3">Thank you for your message!</p>
      )}
      {status === "error" && (
        <p className="text-red-500 font-semibold">
          There was an error submitting your message. Please try again.
        </p>
      )}
      <button
        type="submit"
        className="capitalize text-sm font-medium bg-black text-white px-7 py-3 rounded-full  hover:bg-white hover:border border-black border-solid transition-all duration-150 hover:text-black"
      >
        Submit
      </button>

    </form>
    <Link href={`/AllContact`} className='font-bold text-center text-sm mt-2'>Show User-Contact Details</Link>
    </div>
  );
};

export default ContactForm;
