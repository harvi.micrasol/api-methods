'use client'

import React, { useState } from 'react'

const page = () => {
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [phone, setphone] = useState("");

  const addUser = async()=>{
      let data = await fetch(`http://localhost:3000/api/user`,{
        method:"POST",
        body:JSON.stringify({name,email,phone})
      });
      data = await data.json();
      if(data.success){
        alert("New User Created");
      }
      else{
        alert("plz make input field valid");
      }
  }
  return (
    <div>
    <h1 className="mb-7 font-bold text-3xl">Add User</h1>
    <label>Name:</label>
    <input
      className="border"
      value={name}
      type="text"
      placeholder="enter Name"
      onChange={(e) => setname(e.target.value)}
    />
    <div className="mt-3">
      <label>Email:</label>
      <input
        className="border"
        value={email}
        type="text"
        placeholder="enter email"
        onChange={(e) => setemail(e.target.value)}
      />
    </div>
    <div className="mt-3">
      <label>phone:</label>
      <input
        className="border"
        value={phone}
        type="number"
        placeholder="enter phone"
        onChange={(e) => setphone(e.target.value)}
      />
    </div>
    <button className="border border-solid border-black m-4 px-3" onClick={addUser}>
      Add User
    </button>
  </div>
  )
}

export default page