
import apiReducer from "./slice"; 

import { configureStore } from "@reduxjs/toolkit";

export const store = configureStore({
  reducer: {
    apiReducer,
  },
});