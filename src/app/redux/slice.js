
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState ={
  data: [],
}

// fetchData
export const fetchApiData = createAsyncThunk('api/fetchApiData', async () => {
  const response = await axios.get(`https://659fe0505023b02bfe8aaec3.mockapi.io/user`);
  return response.data;
});


// DeleteData
export const deleteUserData = createAsyncThunk('api/deleteUserData', async (id) => {
  await axios.delete(`https://659fe0505023b02bfe8aaec3.mockapi.io/user/${id}`);
  return id;
});

//AddData

export const addUserData = createAsyncThunk('api/addUserData', async (userData) => {
  const response= await axios.post(`https://659fe0505023b02bfe8aaec3.mockapi.io/user`,userData);
  return response.data;
});

//UpdateData
export const updateUserData = createAsyncThunk('api/updateUserData', async ({ id, updatedData }) => {
  const response = await axios.put(`https://659fe0505023b02bfe8aaec3.mockapi.io/user/${id}`, updatedData);
  return response.data;
});


const apiSlice = createSlice({
  name: 'api',  
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(fetchApiData.fulfilled, (state, action) => {
        state.isLoading = 'false';
        state.data = action.payload;
      })
      .addCase(deleteUserData.fulfilled, (state, action) => {
        state.data = state.data.filter((item) => item.id !== action.payload);
      })
      .addCase(addUserData.fulfilled,(state,action)=>{
        state.data.push(action.payload);
      })
      .addCase(updateUserData.fulfilled, (state, action) => {
        state.data.findIndex((item) => item.id === action.payload.id);
      });
  },  
});

export default apiSlice.reducer;
