'use client'

import React, { useState } from 'react'



const page =  () => {

  const [file, setfile] = useState();
  const uploadimg = async(e) =>{
    e.preventDefault();
      console.log(file);

      const data = new FormData();
      data.set('file',file);
      let result = await fetch('/api/uploadimg',{
        method:"POST",
        body:data
      })
      if(result.success == true){
        alert("Img Uploaded");
      }
      result = await result.json();
      console.log(result);
      if(result.success){
        alert("File Uploaded");
      }
  }
  return (
    <form onSubmit={uploadimg}>
      <input type='file' name='file' onChange={(e)=>setfile(e.target.files[0])}></input>
      <button type='submit' className='border border-black px-3'>Upload</button>
    </form>
  )
}

export default page