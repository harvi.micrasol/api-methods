import DeleteUser from "@/lib/DeleteUser";
import Link from "next/link";

export async function getData() {
  const data = await fetch("http://localhost:3000/api/contacts",{cache:"no-cache"});
  const response = await data.json();
  return response;
}

const page = async () => {
  const fetchUserdata = await getData();
  console.log(fetchUserdata);
  return (
    <>
      {fetchUserdata.result.map((item, index) => (
        <>
          <div key={index} className="flex gap-4">
            <span>Name:{item.username}</span>
            <span>Phone:{item.phone}</span>
            <Link href={`/AllContact/${item._id}`}><span>Edit</span></Link>
            <DeleteUser id={item._id}/>
          </div>
          <br />
        </>
      ))}
    </>
  );
};

export default page;
