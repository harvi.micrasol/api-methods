"use client";

import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { IoMdArrowRoundBack } from "react-icons/io";

const page = ({ params }) => {
  let id = params.slug;
  let router = useRouter();
  const [name, setname] = useState("");
  const [phone, setphone] = useState("");

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    let data = await fetch("http://localhost:3000/api/contacts/" + id);
    data = await data.json();
    console.log(data);
    if (data.success) {
      setname(data.result.username);
      setphone(data.result.phone);
    }
  };

    const update = async () =>{

      let result = await fetch("http://localhost:3000/api/contacts/"+id,{
        method:"PUT",
        body:JSON.stringify({name,phone})
      })
      result = await result.json();
      console.log(result);
      if(result.success){
        alert("data Updated");

        router.replace('/AllContact');
        
      }
      else{
        alert("plz make valid input");
      }
    }

  return (
    <div>
      <div className="flex items-center mb-7 gap-3">
        <button onClick={() => router.back()}>
          <IoMdArrowRoundBack />
        </button>
        <h1 className=" font-bold text-3xl ">Update User Details:</h1>
      </div>

      <label>Name:</label>
      <input
        className="border"
        value={name}
        type="text"
        placeholder="enter Name"
        onChange={(e) => setname(e.target.value)}
      />
      <div className="mt-3">
        <label>phone:</label>
        <input
          className="border"
          value={phone}
          type="number"
          placeholder="enter phone"
          onChange={(e) => setphone(e.target.value)}
        />
      </div>
      <button className="border border-solid border-black m-4 px-3" onClick={update}>
        Update User
      </button>
    </div>
  );
};

export default page;
