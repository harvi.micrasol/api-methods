import Image from 'next/image'
import Link from 'next/link'

export default function Home() {
  return (
    <main className='container mx-auto px-4'>
      <Link href={`/User`} className='font-bold text-3xl '><h2>Users</h2></Link>
      <Link href={`/AddUser`} className='font-bold text-3xl mt-2'><h2>AddUser</h2></Link>
      <Link href={`/UserList`} className='font-bold text-3xl mt-2'><h2>Crud using Mock api</h2></Link>
      <Link href={`/AddContact`} className='font-bold text-3xl mt-2'><h2>Add Contact</h2></Link>
      {/* <Link href={`/DemoRedirect`} className='font-bold text-3xl mt-2'><h2>DemoRedirect</h2></Link> */}

      { console.log(process.env.ADMIN_PW)}
    </main>
  )
}
