'use client'

import Link from "next/link";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { IoMdArrowRoundBack } from "react-icons/io";

const page = ({ params }) => {
  let id = params.slug;

  let router = useRouter();
  const [name, setname] = useState("");
  const [email, setemail] = useState("");

  useEffect(() => {
    getData();
  }, [])  
  

  const getData = async () => {
    const data = await fetch("http://localhost:3000/api/user/"+id);
    const response = await data.json();
    setname(response.result.name);
    setemail(response.result.email);
  };

  const update = async () =>{

    let result = await fetch("http://localhost:3000/api/user/"+id,{
      method:"PUT",
      body:JSON.stringify({name,email})
    })
    result = await result.json();
    console.log(result);
    if(result.success){
      alert("data Updated");
    }
    else{
      alert("plz make valid input");
    }
  }

  return (
    <div>
      <div className="flex items-center mb-7 gap-3">
      <button onClick={()=>router.back()}><IoMdArrowRoundBack /></button>
      <h1 className=" font-bold text-3xl ">Update User Details:</h1>
      </div>
     
      <label>Name:</label>
      <input
        className="border"
        value={name}
        type="text"
        placeholder="enter Name"
        onChange={(e) => setname(e.target.value)}
      />
      <div className="mt-3">
        <label>Email:</label>
        <input
          className="border"
          value={email}
          type="text"
          placeholder="enter email"
          onChange={(e) => setemail(e.target.value)}
        />
      </div>
      <button className="border border-solid border-black m-4 px-3" onClick={update}>
        Update User
      </button>
    </div>
  );
};

export default page;
