
import React from 'react'


async function getData(id){
  const data = await fetch(`http://localhost:3000/api/user/${id}`);
  const response = await data.json();
  return response.result;
}



const page = async ({params}) => {
  const user = await getData(params.slug);
  return (
    <div key={user.id}>
      <h2>User Details</h2>
      <h2> ID:{user.id}</h2>
      <h2>name:{user.name}</h2>
      <h2>email:{user.email}</h2>
      
    </div>
  )
}

export default page