import Deletebtn from "@/app/Deletebtn";
import Link from "next/link";

export async function getData() {
  const data = await fetch("http://localhost:3000/api/user");
  const response = await data.json();
  return response;
}

const page = async () => {
  const fetchdata = await getData();
  return (
    <>
      {fetchdata.map((item, index) => (
        <>
          <div className="flex gap-4">
            <span>
              <Link href={`/User/${item.id}`}>
                {item.name}
              </Link>
            </span>

            <span>
              <Link href={`/User/${item.id}`}>
                {item.email}
              </Link>
            </span>

            <span>
              <Link href={`/User/${item.id}/update`}>Edit</Link>{" "}
            </span>

            <span>
              <Deletebtn id={item.id}/>
            </span>
          </div>
          <br />
        </>
      ))}
    </>
  );
};

export default page;
