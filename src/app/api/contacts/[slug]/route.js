import { connectionSrt } from "@/lib/db";
import ContactUser from "@/lib/model/ContactUser";
import mongoose from "mongoose";
import { NextResponse } from "next/server";

export async function PUT(request,content){
      console.log(content.params.slug,"content");
      const contactId = content.params.slug;
      const filter = {_id:contactId};
      const payload = await request.json();
      await mongoose.connect(connectionSrt);
      
      let result= await ContactUser.findOneAndUpdate(filter,payload);
      return NextResponse.json({result,success:true})
}


export async function GET(request,content){
  console.log(content.params.slug,"content");
  const contactId = content.params.slug;
  const filter = {_id:contactId};
  await mongoose.connect(connectionSrt);
  
  let result= await ContactUser.findById(filter);
  return NextResponse.json({result,success:true})
}


export async function DELETE(request,content){
  console.log(content.params.slug,"content");
  const contactId = content.params.slug;
  const filter = {_id:contactId};
  await mongoose.connect(connectionSrt);
  
  let result= await ContactUser.deleteOne(filter);
  return NextResponse.json({result,success:true})
}