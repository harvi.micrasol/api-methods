import { connectionSrt } from "@/lib/db";
import ContactUser from "@/lib/model/ContactUser";
import mongoose from "mongoose";
import { NextResponse } from "next/server";

export async function GET(){


  let data = [];
  try{
    await mongoose.connect(connectionSrt);
    data = await ContactUser.find();
  }
  catch(e){
    data = {success:false}
  }
  
  return NextResponse.json({result:data,success:true})
}

export async function POST(req){
  const payload = await req.json();
  await mongoose.connect(connectionSrt);

  let contact = new ContactUser(payload);

  const result = await contact.save();

  return NextResponse.json({result,success:true})

}