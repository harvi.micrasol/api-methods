import User from "@/utils/dbConn";
import { NextResponse } from "next/server";

export function GET() {
  const data = User;
  return NextResponse.json(data, { status: 200 });
}

export async function POST(reqest) {
  let payload = await reqest.json();
  console.log(payload.name);
  if (!payload.name|| !payload.email || !payload.phone) {
    return NextResponse.json(
      { result: "Name must be required/email must be required/phone must be required", success: false },
      { status: 400 }
    );
  } else {
    return NextResponse.json(
      { result: "new User Created", success: true },
      { status: 200 }
    );
  }
}


