import User from "@/utils/dbConn";
import { NextResponse } from "next/server";

export function GET(request,content) {
  console.log(content.params.slug,"content");

  const userData = User.filter((item)=>(item.id == content.params.slug))
  return NextResponse.json(
    User.length == 0
      ? { result: "No data found", success: false }
      : { result: userData[0], success: true },
    { status: 200 }
  );
}

export async function PUT(request,content){
    let payload = await request.json();
    payload.id = content.params.slug;
    if (!payload.id|| !payload.name || !payload.email) {
      return NextResponse.json(
        { result: "request data is not valid", success: false },
        { status: 400 }
      );
    } else {
      return NextResponse.json(
        { result: "Success ", success: true },
        { status: 200 }
      );
    }
}

export async function DELETE(request,content){
      const id = content.params.slug;
      if(id){
        return NextResponse.json(
          { result: "User Deleted", success: true },
          { status: 200 }
        );
      }
      else{
        return NextResponse.json(
          { result: "Internal server error,plz try after sometimes", success: false },
          { status: 400 }
        );
      }
}