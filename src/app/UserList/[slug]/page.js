"use client";

import { fetchApiData, updateUserData } from "@/app/redux/slice";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { IoMdArrowRoundBack } from "react-icons/io";
import { useDispatch, useSelector } from "react-redux";

const page = ({ params }) => {
  //   console.log(params.slug);
  const dispatch = useDispatch();
  let router = useRouter();
  const [name, setname] = useState("");
  const [city, setcity] = useState("");

  const responseData = useSelector((state) => state.apiReducer.data);

  useEffect(() => {
    dispatch(fetchApiData());
  }, []);

  const filteredData = responseData.filter((item) => item.id === params.slug);

  const getName = filteredData.length > 0 ? filteredData[0].name : "";
  const getcity = filteredData.length > 0 ? filteredData[0].city : "";

  // Set Api's data on state

  useEffect(() => {
    if (getName && getcity) {
      setname(getName);
      setcity(getcity);
    }
  }, [getName, getcity]);

  //Update user Function

  const updateUser = () => {
    dispatch(updateUserData({ id: params.slug, updatedData: { name, city } }));
    if (name && city) {
      alert("id " + params.slug + " is Updated");
    }
    else{
      alert("plz make valid input");
    }
  };

  return (
    <>
      <div className="flex items-center mb-7 gap-3">
        <button onClick={() => router.back()}>
          <IoMdArrowRoundBack />
        </button>
        <h1 className=" font-bold text-3xl ">Update User Details:</h1>
      </div>

      <div className="px-3">
        <label>Name:</label>
        <input
          className="border"
          value={name}
          type="text"
          placeholder="enter Name"
          onChange={(e) => setname(e.target.value)}
        />
        <div className="mt-3">
          <label>Email:</label>
          <input
            className="border"
            value={city}
            type="text"
            placeholder="enter city"
            onChange={(e) => setcity(e.target.value)}
          />
        </div>
        <button
          className="border border-solid border-black m-4 px-3"
          onClick={updateUser}
        >
          Update User
        </button>
      </div>
    </>
  );
};

export default page;
