"use client";

import { deleteUserData, fetchApiData } from "@/app/redux/slice";
import Link from "next/link";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

const page = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchApiData());
  }, []);

  const responseData = useSelector((state) => state.apiReducer.data);
 
  const deleteUser = (id) => {
    dispatch(deleteUserData(id));
  };

  return (
    <>
      <div className="container mx-auto px-4">
        <h1 className="text-center font-bold text-xl">Data from API</h1>
        <Link href={`/AddUserData`}>
          <button className="border-2 rounded-lg p-2 my-4 border-black border-solid">
            Add UserData
          </button>
        </Link>
        <ul>
          {responseData.map((item) => (
            <div key={item.id}>
              <div className="flex my-2 gap-5">
                <p className="w-4">{item.id}</p>
                <p className="w-60">Name: {item.name}</p>
                <p className="w-60">City: {item.city}</p>
                <Link href={`/UserList/${item.id}`}>
                  <button className="border-2 px-3 rounded-lg border-black">
                    Edit
                  </button>
                </Link>
                <button
                  className="border-2 px-3 rounded-lg border-black"
                  onClick={() => deleteUser(item.id)}
                >
                  Delete
                </button>
              </div>
            </div>
          ))}
        </ul>
      </div>
    </>
  );
};

export default page;
