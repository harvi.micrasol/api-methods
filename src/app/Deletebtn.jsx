'use client'

import { Result } from 'postcss';
import React from 'react'
import { store } from './redux/store';

const Deletebtn = (props) => {
  console.log(props.id);
  const Userid = props.id;
  const deletee = async() =>{
    let data = await fetch("http://localhost:3000/api/user/"+Userid,{
      method:"delete",
    });
    data = await data.json();
  }
  return (
    <div>
      <button onClick={deletee}>Delete</button>
    </div>
  )
}

export default Deletebtn