/** @type {import('next').NextConfig} */
const nextConfig = {
  redirects:async()=>{
    return[{
      source:"/DemoRedirect",
      destination:"/",
      permanent:false
    },
    {
      source:"/DemoRedirect/:slug",
      destination:"/",
      permanent:false
    }]
  }
}

module.exports = nextConfig
